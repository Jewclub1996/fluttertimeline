import 'dart:io';

import 'package:flutter_time_line_app/models/post_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

class PostDB {
  String databaseName;

  PostDB({this.databaseName});

  Future<Database> openDatabase() async {
    Directory appDocumentDirectory = await getApplicationDocumentsDirectory();
    //ทำที่อยู่ไฟล์ database
    String databaseLocationInApp =
        join(appDocumentDirectory.path, this.databaseName);

    // เปิดการเชื่อมต่อ
    DatabaseFactory dbFactory = databaseFactoryIo;
    Database db = await dbFactory.openDatabase(databaseLocationInApp);
    return db;
  }

  Future<int> save(Post post) async {
    var database = await this.openDatabase();
    var postStore = intMapStoreFactory.store('posts');

    //เอาข้อมูลไปใส่ใน database
    var dataId = await postStore.add(database, Post.toJson(post));
    await database.close();
    return dataId;
  }

  Future<List<Post>> loadAllPost() async {
    var database = await this.openDatabase();
    var postStore = intMapStoreFactory.store('posts');
    //ดึงข้อมูล
    var snapshots = await postStore.find(database,
        finder: Finder(sortOrders: [SortOrder(Field.key, false)]));
    var postsList = List<Post>();

    for (var record in snapshots) {
      var post = Post.fromRecord(record);
      postsList.add(post);
    }

    return postsList;
  }
}
