import 'package:flutter/cupertino.dart';
import 'package:flutter_time_line_app/database/post_db.dart';
import 'package:flutter_time_line_app/models/post_model.dart';

class PostProvider with ChangeNotifier {
  List<Post> _posts = [];

  List<Post> get posts => _posts; //ทำให้ component อื่าให้งานได้

  addNewPost(String postMessage) async {
    var post = Post(message: postMessage, createDate: DateTime.now());

    //บันทึกลงใน database ก่อน
    var postDB = PostDB(databaseName: 'app.db');
    await postDB.save(post);

    //ดึงข้อมูล
    var postsFromDB = await postDB.loadAllPost();

    _posts = postsFromDB;

    notifyListeners();
  }

  initData() async {
    var postDB = PostDB(databaseName: 'app.db');
    _posts = await postDB.loadAllPost();

    notifyListeners();
  }
}
